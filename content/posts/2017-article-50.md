---
title: Article 50 Invoked
date: 2017-03-29
---
Article 50 invoked, giving us two years before we leave the EU to negotiate the
future relationship.

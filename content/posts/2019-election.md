---
title: 2019 General Election
date: 2019-12-13
---
The Conservative Party wins with 365 seats, a majority of 80.  This majority and
the previous purge of centrist MPs means that the passing of the Withdrawal
Act is very likely.  A manifesto commitment was to "get Brexit done" and
Johnson repeatedly stated that Brexit will be "done" by the end of 2020 (the
end of the transition period). Rationally speaking, Brexit being "done"
includes future trade agreements being in place: no-deal (or skeleton-deal)
exiting from the EU isn't done because there will be future negotiations to be
done. Simply put: Brexit is done when we're in the sunlit uplands.

Quotes from [the manifesto][1]:
  
> We will keep the UK out of the single market, out of any form of customs
> union, and end the role of the European Court of Justice. [...] we will not extend
> the implementation period beyond December 2020.

[1]: https://assets-global.website-files.com/5da42e2cae7ebd3f8bde353c/5dda924905da587992a064ba_Conservative%202019%20Manifesto.pdf
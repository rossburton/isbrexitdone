---
title: 
date: 2019-12-16
---
> Michael Gove has categorically promised the UK will have a trade deal with the
> EU by the end of next year. The senior cabinet minister became the first in
> Boris Johnson’s cabinet to repeat that pledge after the election, saying
> transitional arrangements would definitely stop at the end of December 2020.

Via [The Guardian][1]

[1]: https://www.theguardian.com/politics/2019/dec/15/michael-gove-promises-brexit-trade-deal-with-eu-by-end-of-2020
---
title: End of the Transition Period
date: 2020-12-31
---
End of the transition period (assuming Withdrawat Agreement passes). Trade
agreements with at least the EU need to be in place.